# Etape 1: Se rendre sur google cloud
[Google_Cloud](https://console.cloud.google.com/welcome)
comme ca
![Etape_1](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-1.png)
# Etape 2: Créer un nouveau projet
Sur le projet 53497 cela ouvre une fenetre qui permet de choisir le projet en cours ou d'en creer un
![Etape_2](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-2.png)
# Etape 3: Choisir un nom puis Creer
![Etape_3](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-3.png)
# Etape 4: seletionner le projet nouvellement creer
![Etape_4](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-4.png)
# Etape 5: Ouvrir le tableau de bord du bord choisi
![Etape_5](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-5.png)
# Etape 6: API et service activée
Choisir l'icone "3 traits" > API et services > API et service activée
![Etape_6](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-6.png)
# Etape 7: On arrive sur cette interface
Normalement, pas de pop-up d'erreur
![Etape_7](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-7.png)
# Etape 8: Choisir identifiants puis configurer l'écran d'autorisation
![Etape_8](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-8.png)
# Etape 9: choisir externe puis creer
![Etape_9](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-9.png)
# Etape 10: Mettre le nom de l'app et votre adresse mail de votre compte
![Etape_10](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-10.png)
# Etape 10 bis: Tout en bas dans developpeur, mettre son adresse aussi
Pas obligatoire, mais c'est mieux pour un projet à plusieurs
![Etape_11](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-11.png)
# Etape 11: Niveau d'accès
![Etape_12](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-12.png)
# Etape 11 bis: Tout en bas > enregistrer et continuer
![Etape_13](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-13.png)
# Etape 12: Tout en bas > enregistrer et continuer
![Etape_14](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-14.png)
# Etape 13: Revenir au tableau de bord
![Etape_15](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-15.png)
# Etape 14: Identifiant > creer des identifiants
![Etape_16](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-16.png)
# Etape 15: Cle ID client OAuth
![Etape_17](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-17.png)
# Etape 16: Aplication de bureau
![Etape_18](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-18.png)
# Etape 17: Nom de l'app à saisir
![Etape_19](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-19.png)
# Etape 18: Télécharger le ficier Json
![Etape_20](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-20.png)
# Etape 19: il se trouve aussi dans ID client Auth
![Etape_21](/samplon-exo_g-form-mysql-fastapi-dash/doc/Etape-21.png)
# Etape 20: Enregistrer le fichier sous le nom: "credentials.json"
Important le mettre dans le dossier avec le script "quickstart.py"
