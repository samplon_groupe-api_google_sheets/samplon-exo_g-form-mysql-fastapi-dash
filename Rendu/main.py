import re
import admin
import mysql.connector
import pandas as pd
import unidecode
from sqlalchemy import create_engine


def insert_pays(engine):
    df = pd.read_excel("list_pays.xlsx")
    print(df)
    df.index += 1
    df.to_sql(name="pays", con=engine, if_exists="append", index_label="id_pays")


def get_spreadsheet_as_csv():
    sheet_id = "1QkUIYiE2nIG-0AyJX-pcPedkUDMS1u-lZWtpkUUsMYY"
    return pd.read_csv(f"https://docs.google.com/spreadsheets/d/{sheet_id}/export?format=csv")
    

def get_id(x):
    # google_voyages
    connection =  mysql.connector.connect(
    host="localhost",
    user=admin.user, # Fichier admin.py non-joint sur le git, fichier à creer en local
    password=admin.password, # Fichier admin.py non-joint sur le git, fichier à creer en local
    database="google_voyages")

    cursor = connection.cursor(dictionary=True)
    cursor.execute("""SELECT id_pays FROM pays WHERE pays = %s LIMIT 1""", [x])
    result = cursor.fetchone()
    cursor.close()
    connection.close()
    if result:
        return int(result['id_pays'])
    return None


def if_number(x):
    try:
        return int(float(x))
    except:
        return None


if __name__ == '__main__':
    engine = create_engine(f"mysql+pymysql://{admin.user}:{admin.password}@localhost/google_voyages", echo=False)
    try:
        insert_pays(engine)
    except:
        pass
    df = get_spreadsheet_as_csv()
    df.rename(columns={k: unidecode.unidecode(re.search(r"(\w+\s?)+?$", k.lower()).group(0)).replace(" ", "_") for k in
                       df.columns}, inplace=True)
    df.rename(columns={"pays": "pays_name", "duree_du_voyage": "duree", "pays_de_residence": "pays_residence_name"},
              inplace=True)
    df.drop(columns=["horodateur"], inplace=True)
    df.dropna(subset="pays_name", inplace=True)
    df.index += 1
    
    df_personne = df[["pays_residence_name", "nom", "prenom"]].groupby(["nom", "prenom", "pays_residence_name"]).apply(
        lambda x: list(x.index))
    df_personne.name = "id_from_df"
    df_personne = df_personne.reset_index()
    df_personne.index += 1
    df_personne["pays_residence_name"] = df_personne["pays_residence_name"].apply(
        lambda x: re.search(r"([a-z-]+\s?)+?$", x.lower()).group(0))
    df_personne["pays_residence"] = df_personne["pays_residence_name"].apply(get_id)
    df["pays_name"] = df["pays_name"].apply(lambda x: re.search(r"([a-z-]+\s?)+?$", x.lower()).group(0))
    df["pays"] = df["pays_name"].apply(get_id)
    df_personne = df_personne.dropna(subset="pays_residence")
    df = df.dropna(subset="pays")
    try:
        df_personne[["nom", "prenom", "pays_residence"]].to_sql(name="personnes", con=engine, index_label="id_personne",
                                                                if_exists="append")
    except:
        pass
    df_personne = df_personne.explode("id_from_df")
    df_personne.reset_index(inplace=True)
    df.reset_index(inplace=True)
    df_voyage = df_personne.merge(df, how="inner", left_on="id_from_df", right_on="index")[["index_x", "pays", "duree"]]
    df_voyage.rename(columns={"index_x": "id_personne", "pays": "id_pays"}, inplace=True)
    df_voyage["duree"] = df_voyage["duree"].apply(lambda x: x.replace(",", ".")).apply(if_number)
    df_voyage.dropna(subset="duree", inplace=True)
    df_voyage.to_sql("voyage", engine, if_exists="append", index=False)