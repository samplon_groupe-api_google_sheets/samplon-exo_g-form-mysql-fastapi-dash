def list_to_dict(list_column, list_value):
    """Transforme une liste en dictionnaire
    Args:
        list_column (_type_): _description_
        list_value (_type_): _description_
    Returns:
        _type_: dictionnaire
    """
    # dict_from_list = dict(zip(list_column, list_value))
    dictionnary = {}
    for key, value in zip(list_column,list_value):
        dictionnary[key] = value
    return dictionnary


def format_colum(data: str):
    return data[7:]