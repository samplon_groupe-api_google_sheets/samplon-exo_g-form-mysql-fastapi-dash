DROP DATABASE IF EXISTS google_voyages;
CREATE DATABASE google_voyages;
USE google_voyages;

CREATE TABLE pays (
  id_pays INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  pays VARCHAR(100) NOT NULL,
  UNIQUE (pays)
);

CREATE TABLE personnes (
  id_personne INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  prenom VARCHAR(100) NOT NULL,
  nom VARCHAR(100) NOT NULL,
  pays_residence INT NOT NULL,
  FOREIGN KEY (pays_residence) REFERENCES pays (id_pays)
);

CREATE TABLE voyage (
  id_personne INT,
  id_pays INT,
  duree INT,
  FOREIGN KEY (id_personne) REFERENCES personnes (id_personne),
  FOREIGN KEY (id_pays) REFERENCES pays (id_pays)
);


DROP USER IF EXISTS lolam1@localhost;
CREATE USER lolam1@localhost IDENTIFIED BY 'lolam1';
GRANT ALL PRIVILEGES ON google_voyages.* TO 'lolam1'@'localhost';
FLUSH PRIVILEGES;



