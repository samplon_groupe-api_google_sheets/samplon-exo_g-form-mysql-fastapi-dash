# EXO GFROUPE Samplon-Exo_G Form-Mysql-FastAPI-Dash
Contexte du projet
On veut faire un sondage sur des données de voyage.
Suivre le modèle des données
Documentation commun toutes les groupes:
[Check lien](https://mkdoc-voyage-alixpaltik-f2829af6bb495743eb2de6b70cc0d31e8851e4f.gitlab.io/VOYAGE/#groupe-1)

## 1er partie: Groupe B Google API to Json, DF, Mysql
Utilisez l’API de Google sheets pour récupérer les réponses au formulaire à partir d’un script python

### MLD
![Alt text](Exo_voyage_Google_Dash.png)

### MPD
File init.sql

### Interface with group A:
The link to the googleForm to collect data for the project:
[Check lien](https://docs.google.com/forms/d/e/1FAIpQLScHZ1dZZ9vn60Uqmv6wPdQOf10GazkCNqbPzMIxCo8DmPuKcw/viewform)

The googleSheet link storing the data from the form:
[Check lien](https://docs.google.com/spreadsheets/d/11_TY-OTM8LryGQ7GNlGvfTDqVaiIBkEj9ge-ZwkZ-qQ/edit#gid=2115555633)

### Connection to GoogleSheets:

Using an API to connect to google workspace
[Check lien](https://developers.google.com/sheets/api/quickstart/python)












